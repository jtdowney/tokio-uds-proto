extern crate futures;
extern crate tokio_core;
extern crate tokio_proto;
extern crate tokio_service;
extern crate tokio_uds_proto;

use futures::{Future, IntoFuture};
use futures::future::FutureResult;
use tokio_core::io::{Io, Codec, EasyBuf, Framed};
use tokio_core::reactor::Core;
use tokio_proto::pipeline::{ServerProto, ClientProto};
use tokio_service::Service;
use tokio_uds_proto::{UnixClient, UnixServer};
use std::fs;
use std::{io, str};
use std::thread;
use std::time::Duration;

#[test]
fn test_simple_echo() {
    let mut core = Core::new().unwrap();

    // This brings up our server.
    let path = "./tests/socket";

    thread::spawn(move || { UnixServer::new(LineProto, path.into()).serve(|| Ok(EchoService)); });

    // A bit annoying, but we need to wait for the server to connect
    thread::sleep(Duration::from_millis(100));

    let handle = core.handle();
    let client = UnixClient::new(LineProto).connect(path, &handle).unwrap();
    core.run(client.call("ping".into())
            .and_then(|response| if response == "ping" {
                Ok(())
            } else {
                Err(io::Error::new(io::ErrorKind::InvalidData, "wrong response"))
            }))
        .unwrap();

    fs::remove_file(path).unwrap();
}

#[test]
#[should_panic(expected = "wrong response")]
fn test_simple_changed_echo() {
    let mut core = Core::new().unwrap();

    // This brings up our server.
    let path = "./tests/socket2";

    thread::spawn(move || { UnixServer::new(LineProto, path.into()).serve(|| Ok(EchoService)); });

    // A bit annoying, but we need to wait for the server to connect
    thread::sleep(Duration::from_millis(100));

    let handle = core.handle();
    let client = UnixClient::new(LineProto).connect(path, &handle).unwrap();
    let res = core.run(client.call("ping".into())
        .and_then(|response| if response == "pong" {
            Ok(())
        } else {
            Err(io::Error::new(io::ErrorKind::InvalidData, "wrong response"))
        }));

    fs::remove_file(path).unwrap();
    res.unwrap();
}

/// Our line-based codec
pub struct LineCodec;

/// Protocol definition
struct LineProto;

impl Codec for LineCodec {
    type In = String;
    type Out = String;

    fn decode(&mut self, buf: &mut EasyBuf) -> Result<Option<String>, io::Error> {
        // Check to see if the frame contains a new line
        if let Some(n) = buf.as_ref().iter().position(|b| *b == b'\n') {
            // remove the serialized frame from the buffer.
            let line = buf.drain_to(n);

            // Also remove the '\n'
            buf.drain_to(1);

            // Turn this data into a UTF string and return it in a Frame.
            return match str::from_utf8(&line.as_ref()) {
                Ok(s) => Ok(Some(s.to_string())),
                Err(_) => Err(io::Error::new(io::ErrorKind::Other, "invalid string")),
            };
        }

        Ok(None)
    }

    fn encode(&mut self, msg: String, buf: &mut Vec<u8>) -> io::Result<()> {
        buf.extend(msg.as_bytes());
        buf.push(b'\n');

        Ok(())
    }
}

impl<T: Io + 'static> ClientProto<T> for LineProto {
    type Request = String;
    type Response = String;

    /// `Framed<T, LineCodec>` is the return value of `io.framed(LineCodec)`
    type Transport = Framed<T, LineCodec>;
    type BindTransport = Result<Self::Transport, io::Error>;

    fn bind_transport(&self, io: T) -> Self::BindTransport {
        Ok(io.framed(LineCodec))
    }
}

impl<T: Io + 'static> ServerProto<T> for LineProto {
    type Request = String;
    type Response = String;

    /// `Framed<T, LineCodec>` is the return value of `io.framed(LineCodec)`
    type Transport = Framed<T, LineCodec>;
    type BindTransport = Result<Self::Transport, io::Error>;

    fn bind_transport(&self, io: T) -> Self::BindTransport {
        Ok(io.framed(LineCodec))
    }
}

struct EchoService;
impl Service for EchoService {
    type Request = String;
    type Response = String;
    type Error = io::Error;
    type Future = FutureResult<String, io::Error>;

    fn call(&self, req: Self::Request) -> Self::Future {
        Ok(req).into_future()
    }
}
